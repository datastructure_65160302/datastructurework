import java.util.Arrays;

public class mergeNum {
    static int[] nums1 = {1,2,3,0,0,0};
    static int[] nums2 ={2,5,6};
    static int m = 3;
    static int n = 3;

    static void mergeNums(){
        for(int i= 0;i < n;i++){
            nums1[i+m] = nums2[i];
        }
    }

    static void printNumsfinal(){
        System.out.print("The result of the merge is ");
        for(int i =0;i<nums1.length;i++){
            System.out.print(nums1[i]+" ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        mergeNums();
        Arrays.sort(nums1);
        printNumsfinal();
    }
}
