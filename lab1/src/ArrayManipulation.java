
import java.util.Arrays;

public class ArrayManipulation {

    public static void main(String[] args) {
        int[] numbers ={5,8,3,2,7};
        String[] names ={"Alice","Bob","Charlie","David"};
        double[] values = new double[4];
        int result = 0;
        double max = 0.0;
        String[] reverseName = new String[4];

        for(int i = 0;i < numbers.length; i++){
            System.out.print(numbers[i]+ " ");
        }
        System.out.println();

        for(int i = 0;i < names.length;i++){
            System.out.print(names[i]+ " ");
        }
        System.out.println();

        for(int i =0;i < values.length;i++){
            values[i] = i;
            System.out.print(values[i] + " ");
        }
        System.out.println();

        for(int i = 0;i < numbers.length;i++){
            result = result + numbers[i];
        }
        System.out.println("Sum of all elements in numbers = " + result);

        for(int i = 0;i < values.length;i++){
            max = values[0];
            if(values[i] > max){
                max = values[i];
            }
        }
        System.out.println("Max values is "+max);

        for(int i =0;i < names.length;i++){
            reverseName[i] = names[3-i];
            System.out.print(reverseName[i]+" ");
        }
        System.out.println();

        Arrays.sort(numbers);
        for(int i =0;i < numbers.length;i++){
            System.out.print(numbers[i]+" ");
        }
    }
}
