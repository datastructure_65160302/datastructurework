public class testTraverse {


    public static void main(String[] args) {
        Tree theTree = new Tree();
        theTree.insert(50, 1.5);
        theTree.insert(25, 1.7);
        theTree.insert(75, 1.9);
        theTree.insert(80, 1.1);
        theTree.insert(35, 1.2);
        theTree.insert(45, 1.3);

        theTree.traverse(1);
        theTree.traverse(2);
        theTree.traverse(3);
    }
}
