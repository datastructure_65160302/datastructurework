import java.util.Scanner;

public class StackApp {
    public static void main(String[] args) {
        StackX theStack = new StackX(10);
        while( !theStack.isFull()){
            Scanner sc = new Scanner(System.in);
            int num = sc.nextInt();
            theStack.push(num);
            System.out.println("Top of Stack is "+theStack.peek());
        }
        System.out.println("Stack is full.");

        while( !theStack.isEmpty()){
            long value = theStack.pop();
            System.out.println("Pop num is "+value);
        }
    }
}
