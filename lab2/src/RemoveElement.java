public class RemoveElement {
    static int[] nums = {0,1,2,2,3,0,4,2};
    static int val = 2;
    static int remove(int[] nums, int val){
        int count = 0;
        for(int i = 0;i < nums.length;i++){
            if(nums[i] != val){
                nums[count] = nums[i];
                count++;
            }
        }
        return count;
    }

    static void printNum(){
        for(int i = 0;i < nums.length;i++){
            System.out.print(nums[i] + " ");
        }
        System.out.println();
    }
    public static void main(String[] args) {
        System.out.println(remove(nums, val));
        printNum();
    }
}
