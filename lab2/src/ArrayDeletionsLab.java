public class ArrayDeletionsLab {
    static int[] arr = {1, 2, 3, 4, 5} ;
    static int[] newarr = new int[arr.length - 1];
    static int[] valuearr = new int[newarr.length-1];
    static int deleteIndex = 2;
    static int value = 4;

    static int[] deleteElementByIndex(int[] arr, int index){
        for(int i = 0; i < deleteIndex; i++){
            newarr[i] = arr[i];
        }
        for (int i = deleteIndex +1; i < arr.length; i++){
            newarr[i - 1] = arr[i];
        }
        return newarr;
    }

    static void printnewArray(){
        for(int i = 0;i < newarr.length;i++){
            System.out.print(newarr[i] + " ");
        }
        System.out.println();
    }

    static int[] deleteElementByValue(int[] arr, int value){
        int i = 0;
        while(newarr[i] != 4){
            valuearr[i] = newarr[i];
            i++;
        }
         for (int j = i+1; j < newarr.length; j++){
            valuearr[j-1] = newarr[j];
        }
        return valuearr;  
    }

    static void printvaluearr(){
        for(int i = 0;i < valuearr.length;i++){
            System.out.print(valuearr[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args){
        deleteElementByIndex(arr, deleteIndex);
        printnewArray();
        deleteElementByValue(arr, value);
        printvaluearr();
    }
}
